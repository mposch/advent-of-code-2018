package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	f, err := os.Open("./input.txt")
	check(err)

	counter := 0

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		textValue := scanner.Text()
		intValue, err := strconv.Atoi(textValue)
		check(err)

		counter += intValue
	}
	fmt.Println(counter)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

}
