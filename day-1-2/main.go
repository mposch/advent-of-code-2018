package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func Extend(slice []int, element int) []int {
	n := len(slice)
	if n == cap(slice) {
		newSlice := make([]int, len(slice), 2*len(slice)+1)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}

func CheckIfDoubled(list []int) bool {
	valueCounter := 0
	valueToCheck := list[len(list) - 1]
	contains := false
	for _, element := range list {
		if element == valueToCheck {
			valueCounter++
		}

		if valueCounter == 2 {
			contains = true
		}
	}

	return contains
}

func LoopList(fileName string, valueList []int, counter int, i int) {
	f, err := os.Open(fileName)
	check(err)

	i++
	fmt.Println("iteration: ", i)

	scanner := bufio.NewScanner(f)
	contains := false
	for scanner.Scan() {
		textValue := scanner.Text()
		newValue, err := strconv.Atoi(textValue)
		check(err)

		lastValue := valueList[len(valueList) - 1]
		valueList = Extend(valueList, lastValue +newValue)

		contains = CheckIfDoubled(valueList)
		if contains {
			fmt.Println("CONTAINS", counter)
			break
		}
	}

	if !contains {
		LoopList(fileName, valueList, counter, i)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	fileName := os.Args[1]

	valueList := make([]int, 0)
	valueList = Extend(valueList, 0)

	counter := 0
	LoopList(fileName, valueList, counter, 0)
}
